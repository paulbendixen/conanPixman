#include <iostream>
#include "pixman.h"

int main() {
    pixman_glyph_cache_t *cache = pixman_glyph_cache_create();
    pixman_glyph_cache_destroy( cache );
}
