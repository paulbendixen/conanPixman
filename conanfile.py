from conans import ConanFile, AutoToolsBuildEnvironment, tools


class PixmanConan(ConanFile):
    name = "Pixman"
    version = "0.34.0"
    license = ["LGPL v2.1", "MPL 1.1"]
    url = "https://gitlab.com/paulbendixen/conanPixman"
    description = "Pixman package for cairo dependency"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=True"
    generators = "cmake"

    def configure(self):
        del self.settings.compiler.libcxx

    def source(self):
        tools.get( "https://www.cairographics.org/releases/pixman-{}.tar.gz".format( self.version ) )

    def build(self):
        maker = AutoToolsBuildEnvironment( self )
        maker.configure( configure_dir="pixman-{}".format( self.version ) )
        maker.make()
        maker.install()

    def package(self):
        self.copy("*.h", dst="include", src="pixman-{}/pixman/".format( self.version ) )
        self.copy("*.la", dst="lib", src="pixman" )
        self.copy("*hello.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["pixman-1"]

